# CHIP-2021-02 Unforgeable Groups for Bitcoin Cash

        Title: Unforgeable Groups for Bitcoin Cash
        First Submission Title: Group Tokenization for Bitcoin Cash
        First Submission Date: 2021-02-23
        Owners: bitcoincashautist (ac-A60AB5450353F40E)
        Type: Technical
        Layers: Consensus, Script, Network
        Status: DRAFT
        Current Version: 7.0
        Last Edit Date: 2022-06-05


## Contents

1. [Summary](#summary)
2. [Deployment](#deployment)
3. [Benefits](#benefits)
4. [Technical Description](#technical-description)
5. [Specification](#specification)
6. [Security Analysis](#security-analysis)
7. [Implementations](#implementations)
8. [Test Cases](#test-cases)
9. [Activation Costs](#activation-costs)
10. [Ongoing Costs](#ongoing-costs)
11. [Costs and Benefits Summary](#costs-and-benefits-summary)
12. [Risk Assessment](#risk-assessment)
13. [Evaluation of Alternatives](#evaluation-of-alternatives)
14. [Discussions](#discussions)
15. [Statements](#statements)
16. [Changelog](#changelog)
17. [Copyright](#copyright)
18. [Credits](#credits)
19. [Appendix - Group Logic Pseudo Code](#appendix-group-logic-pseudo-code)

## Summary

[[Back to Contents](#contents)]

[This proposal](#) describes an upgrade to Bitcoin Cash peer-to-peer electronic cash system that enables **unforgeable contracts** and **native tokens**.
Those features will scale as well as current Bitcoin Cash features and will not interfere with Bitcoin Cash scaling or its utility as peer-to-peer cash.
The upgrade will be deployed as part of Bitcoin Cash upgrade schedule, that allows for clean upgrades using orderly hard-forks.
Only validating node software needs to be upgraded while other applications won't require an upgrade, meaning everyone can continue using Bitcoin Cash (BCH) as they're used to, no action required.
Upgrading old applications or creating new ones will be on a voluntarily basis by those that want to access new features, and at their own pace.
We expect that many will be attracted to build and access products using those features, and that such products will bring more utility to Bitcoin Cash.

[Motivation](#motivation) section will show the limitation of current Bitcoin Cash blockchain capabilities in that it currently can not create contract instances that can't be counterfeited.

[Benefits](#benefits) section will show why those features are essential in facilitating a parallel economy founded on Bitcoin Cash.
Money is always exchanged for something, be it a good or a service.
The proposed features will allow units of BCH to create persistent blockchain structures, programmable pots of money that can not be counterfeited, that can be made to interact with each other, thus increasing utility of BCH.
Using those programmable pots of money, goods and services may be represented natively on the blockchain so that they can be trustlessly exchanged for cash, Bitcoin Cash.
Even some business processes may live as interconnected pots of money, so that organizations could transparently and trustlessly guarantee delivery of cash to those that can prove a claim.
This will enable creation of organization with a great degree of decentralization and censorship resistance.

[Technical description](#technical-description) section will show how this will be achieved by natural extension of Bitcoin Cash UTXO blockchain model.

[Security analysis](#security-analysis) section will demonstrate soundness of the proposed system, and show it doesn't introduce new DoS or malleability vectors.

[Activation costs](#activation-costs) section will demonstrate that it is mandatory only for node software to upgrade, and unupgraded non-node software can continue to function without an upgrade.
Only accessing new features will require an upgrade of other software, where it is expected that the cost of such upgrade will be offset by the benefits of new features.

[Ongoing costs](#ongoing-costs) section will demonstrate that the proposal doesn't change the scaling "big O", and that it doesn't introduce new scaling bottlenecks or node operating costs.

[Costs and benefits summary](#costs-and-benefits-summary) will show how this upgrade brings ever-growing benefits to all stakeholders while keeping activation costs fixed and contained to a scheduled node software upgrade.

[Risk assessment](#risk-assessment) will demonstrate that the upgrade is by nature low risk, because it is well contained and risk factors are entirely controlled by node developers.

## Deployment

[[Back to Contents](#contents)]

This proposal targets May 2023 activation.
It has dependencies in:

- [CHIP-2021-02: Native Introspection Opcodes](https://gitlab.com/GeneralProtocols/research/chips/-/blob/master/CHIP-2021-02-Add-Native-Introspection-Opcodes.md), which will be extended by this proposal with additional opcodes.

## Motivation

[[Back to Contents](#contents)]

{TODO}

## Benefits

[[Back to Contents](#contents)]

{TO BE UPDATED}

### Unforgeable Contracts

By including genesis input's prevout locking script in the preimage which generates a new group identifier, a new class of contracts will be enabled.
These contracts will be able to prove, using a compact fixed-size proof, that they have been set at group genesis and any descendant outputs must have inherited from the first contract.
In other words, it will become possible to construct contracts that would be able to prove their ancestry, and because of that they will be unforgeable contracts.

### Native Token Primitives Super-contract

Here we propose to implement native token primitives as a hard-coded "super-contract", that is to be enforced directly by consensus code and applied to whole groups by induction, i.e. by consistently applying the super-contract for each transaction.

If only the "unforgeable contracts" feature would be enabled then it would become theoretically possible to implement fungible and non-fungible tokens using Script contracts relying on unforgeability of the group identifier.
Apart from proving its own authenticity, such contract would have to verify that exactly one UTXO is being created (NFT), or verify that token amounts balance across the transaction (FT).
Because Script doesn't support for-each loops, the contract would have to limit the number of outputs in the TX and have a line of code per each output.
To be able to change ownership would require either a self-mutating contract that would have to verify a fixed number of ownership templates such as P2PKH and 2-of-3 multisig, or entangling the contract with another output that would authenticate spending from the contract output.
Every token contract would need to have this boilerplate code, and every transfer would need to replicate it.

#### Non-fungible Tokens

NFTs are essential, not only as tradable items, but for interaction with other contracts.
For example, they can be used as another contract's access control, where the "main" contract would require the NFT to be spent in the same transaction.
Proving that some Script really behaves like an NFT would require the "main" contract to reconstruct the contract template.

These complexities and overheads can be avoided by enforcing a super-contract over the whole group, allowing Script authors to focus on implementing contract logic instead of writing convoluted Script code just to verify that an NFT is really an NFT.

##### Non-fungible Token Messages

NFTs fundamental use is as messengers between contracts.
The genesis setup already allows us to compress any message "inside" the `groupID` but that comes with the caveat of requiring a new genesis operation and passing on the whole main covenant from old `groupID` to new `groupID`.

Enabling a dedicated message field will allow contracts to more easily emit and receive messages from other contracts, which we feel will be an important building block in complex multi-group and multi-output contracts that can benefit from massive parallelization and scaling that Bitcoin's UTXO model can support.

#### Fungible Tokens

The market has already seen a token Cambrian explosion, and demand for tokens is seen in growth of [tokens market cap](https://coinmarketcap.com/tokens/views/all/) and market appreciation of native currencies that power them.
The era of tokenization of everything has started with Ethereum and it's only been ramping up.

Enforcing token logic as a super-contract would enable tokens to behave just like BCH:

- Tokens that are "free" P2PKH citizens and can therefore be locked with any Bitcoin Cash Script.
- Easy for usage, because no new address format required and a single address can receive multiple tokens.
- Easy to upgrade wallets and middleware because token transactions are built almost the same as BCH transactions.
- Easy to parse the blockchain to build databases and track token stats.
- Easy to prove state of token supply, SPV proof sufficient for some kinds.
- Fully compatible with 0-conf and SPV wallets.
- Scaling function, the "big O", unchanged.
- Cheap on node resources, almost as cheap as BCH.
They can be thought of as heavier BCH outputs.
- Fast as BCH, secure as BCH, and almost as cheap to transact with.
- Atomic swaps, decentralized exchange, and token CashFusion possible through existing BCH transaction signing mechanism.
- Complex contract interaction possible through inductive proof solutions such as PMv3's "detached proof" feature.

Having the native token feature would therefore give us the best of both worlds: flexibility of Script, and conveniences of BCH P2PKH outputs.

#### Synergistic Benefit

Script covenants can be set to require a token genesis to unlock, resulting in a "free" P2PKH token that carries proof of whatever conditions were required for the genesis transactions.

Another contract can later verify the token by rebuilding the script template and verifying it against the cryptographic commitment (group identifier) carried by the token.

This allows for constructions such as BCH backed tokens, created by a Script requiring the genesis TX to pay into a public covenant, and the token could later redeem from *any* covenant instance (UTXO).

One example of this is a "pre-genesis covenant":

- Require that 10,000 sats is paid into some P2SH address and a token with supply of 1 created.
The target covenant later requires a proof of burn of any token matching the genesis template and releases the 10,000 sats.
Because the covenant only verifies the genesis template and not a fixed ID, any token owner can take 10,000 from any covenant UTXO, even ones created by others.
Proof of concept example can be found [here](https://ide.bitauth.com/import-gist/aa990c4c0012b24a8b04723bbbb3d00e).

## Technical Description

[[Back to Contents](#contents)]

Bitcoin Cash already is programmable cash, where every unit of cash carries a program *(a "smart contract")* that determines how it may be spent.
Most common such program is known as an "address", which simply requires a signature in order to spend the cash unit.
We will first introduce the concept of **unforgeable contracts**, that enables units of BCH to be marked with an unique identifier which will serve as proof that some BCH amount came from a particular contract instance.
Bitcoin Cash units that inherit from the same unforgeable contract instance can be thought of as forming an exclusive group of cash units, where membership is controlled exclusively by current members, thus we title this proposal **unforgeable groups** *(of BCH outputs)*.
We will then use that as a fundamental feature on top of which to build **native tokens** through introducing two essential "super-contracts", non-fungible token (NFT) and fungible token (FT) contract primitives enforced by native consensus code, that can be seamlessly used by BCH contracts to create efficient token systems tailored to their purpose.

[Transaction format](#transaction-format) will be extended to allow [transaction outputs](#grouped-output-format) to optionally carry an identifier using the non-breaking [**P**re**F**i**X**](https://gitlab.com/bitcoin-cash-node/announcements/-/blob/master/2021-12-23_evaluate_viability_of_transaction_format_or_id_change_EN.md#an-alternative-to-breaking-change) method.
We will refer to all transaction outputs (TXOs) having a common identifier as a group *(of BCH outputs)*, and individual such outputs as grouped outputs.
New outputs will be allowed into an existing group only by spending an unspent transaction output (UTXO) of the same group in the same transaction, [allowing only existing members to introduce new members](#group-membership).
Transaction [input format](#group-genesis-input-format) will be extended similarly to outputs, so inputs may be spent as group genesis inputs.
Such inputs will be the only way to allow creation of outputs with a newly generated group identifier.
The identifier will be deterministically generated based on the input's unique data, therefore every [group genesis](#group-genesis) transaction will be unique.
By induction, every group will be exclusive to descendants of the genesis input, therefore group membership will be unforgeable.

Additionally, the format will allow grouped outputs to encode token state using a fixed-size [output type](#group-output-type) field, and two optional data fields: a fixed-size fungible token (FT) amount, and a variable-length non-fungible token (NFT) message.
Genesis input format will allow encoding the same fields which will be used to initialize a new group's token state.
Native tokens will be enabled by two [native token "super-contracts"](#native-token-super-contracts), kind of contract directly enforced by consensus code, that will place immutable rules on how token state may evolve across any single transaction involving grouped outputs.
The enforcement will be transaction-local, and immutable group properties will be globally guaranteed by induction:

- unforgeability of the group identifier,
- non-fungible tokens cardinality (number of outputs),
- non-fungible tokens message authenticity,
- fungible tokens accounting equation.

[Introspection opcodes] will be extended to access grouped output fields and input genesis declaration.

[Transaction signing](#transaction-signing) will be updated with a new signature hash type which will be mandatory for signing transactions involving grouped outputs.

[Network rules](#network-rules) will be updated to allow transactions with grouped output templates to be relayed while the network message format will remain the same.

[Wallet address format](#address-format) update will be discussed and shown to be unnecessary.

### Transaction Format

We will extend both input and output format using the non-breaking [**P**re**F**i**X**](https://gitlab.com/bitcoin-cash-node/announcements/-/blob/master/2021-12-23_evaluate_viability_of_transaction_format_or_id_change_EN.md#an-alternative-to-breaking-change) method and will refer to the full input prefix with its fields as "group genesis input annotation", and the full output prefix with its fields as "group annotation".
We will use the same `0xEE` prefix byte for both prefixes, which is not conflicting because input's `unlocking script` and output's `locking script` are isolated contexts when parsing transactions.

#### Grouped Output Format

* transaction outputs
    * output 0
        * satoshi amount, 8-byte uint
        * locking script length, [compact-size uint](https://reference.cash/protocol/formats/variable-length-integer)
        * locking script
            * **OPTIONAL: PFX_GROUP**, 1-byte constant `0xEE`
                * group ID, 32 raw bytes
                * token type, 1 raw byte
                * OPTIONAL: fungible token amount, [compact-size uint](https://reference.cash/protocol/formats/variable-length-integer)
                * OPTIONAL: nft message length, [compact-size uint](https://reference.cash/protocol/formats/variable-length-integer)
                * OPTIONAL: nft message, variable number of raw bytes
            * real locking script, variable number of raw bytes
    * ...
    * output N

Where bit 0 (`0x01`) of `token type` will indicate whether `fungible token amount` field will be used, and bit 4 (`0x10`) will indicate whether `nft message length` and `nft message` fields will be used.

Range of `fungible token amount` will be limited to the positive part of signed 64-bit integer range, so that every amount may be read by Script VM *(using transaction introspection opcodes)* and manipulated as a Script number.

Similarly, size of `nft message` will be limited by the [`MAX_SCRIPT_ELEMENT_SIZE`](https://gitlab.com/bitcoin-cash-node/bitcoin-cash-node/-/blob/master/src/script/script.h#L23) limit, so that every `nft message` may be read and manipulated by Script VM.

#### Group Genesis Input Format

Field definitions will match grouped output definitions and will serve as new group's state initialization.
The `group ID` field is deterministically generated so it will be replaced by the `group genesis hash type` field that will indicate how the new group's ID is to be generated.

* transaction inputs
    * input 0
        * previous output transaction hash, 32 raw bytes
        * previous output index, 4-byte uint
        * unlocking script length, [compact-size uint](https://reference.cash/protocol/formats/variable-length-integer)
        * unlocking script
            * **OPTIONAL: PFX_GROUP_GENESIS**, 1-byte constant `0xEE`
                * group genesis hash type, 1 raw byte
                * initial token type, 1 raw byte
                * OPTIONAL: initial fungible token amount, [compact-size uint](https://reference.cash/protocol/formats/variable-length-integer)
                * OPTIONAL: initial nft message length, [compact-size uint](https://reference.cash/protocol/formats/variable-length-integer)
                * OPTIONAL: initial nft message, variable number of raw bytes
            * real unlocking script, variable number of raw bytes
        * sequence number, 4-byte uint
    * ...
    * input N

Past versions of this proposal have inferred genesis from the TX construction, and then verified it using rigidly defined prevout(s) as a source of entropy for generating and validating the `group ID` set on first output(s) of a newly created group.

We see the above explicit genesis declaration as advantageous because:

- it simplifies consensus rules verification, because group consensus code can completely ignore non-annotated inputs and outputs,
- it is consistent with Bitcoin design, where it is the input which allows creation of something from nothing (coinbase input),
- it simplifies validating a new group's initial token state from within Script VM,
- it allows for multiple modes of generating the group ID,
- it is non-malleable, because genesis will require at least one associated output to be created, and associated outputs may be signed.

#### Token Type

Every grouped output will encode either a non-fungible token (NFT), a fungible token (FT) amount, or both.
Output's `token type` *(and the related input `initial token type` field)* will encode what is the case.
If token type encodes a NFT, then the field will also encode:

- the NFT's capability, and
- whether the output encodes a NFT message.

If there are no tokens, then the entire group annotation will be omitted and the output will be a pure BCH output.

We have already defined bit flags `0x01` and `0x10`, that will indicate whether associated optional fields will be encoded:

- `HAS_FT_AMOUNT = 0x01`,
- `HAS_NFT_MESSAGE = 0x10`.

Those have been defined first because they're required for correctly parsing the transaction format.

Other defined bit flags are:

- `NFT_EDITABLE = 0x20`,
- `NFT_CLONEABLE = 0x40`,
- `HAS_NFT_STATE = 0x80`.

Undefined bits are reserved and must be 0 therefore `group_output_type & 0x0E != 0x00` will fail the TX.
The NFT message field can not be used unless the output encodes an NFT therefore `group_output_type & 0x90 == 0x10` will fail the TX.
Also, the output may have a NFT capability only if it is an NFT therefore `group_output_type & 0xE0 != 0 && group_output_type & 0x80 == 0` will fail the TX.

Here we're only concerned with transaction format so we have only defined the fields and flags to be used and have not described their purpose, which will be done in the next section.

##### Token Type Allowed Values

For clarity, the below table lists only allowed values of the `token_type` field and their meaning.

token_type | bit7 | bit6 | bit5 | bit4 | bit3 | bit2 | bit1 | bit0 | Note
-- | -- | -- | -- | -- | -- | -- | -- | -- | --
0x01 | 0 | 0 | 0 | 0 | 0 | 0 | 0 | 1 | No NFT, no message, has FT
0x80 | 1 | 0 | 0 | 0 | 0 | 0 | 0 | 0 | Common NFT, no message, no FT
0x81 | 1 | 0 | 0 | 0 | 0 | 0 | 0 | 1 | Common NFT, no message, has FT
0x90 | 1 | 0 | 0 | 1 | 0 | 0 | 0 | 0 | Common NFT, has message, no FT
0x91 | 1 | 0 | 0 | 1 | 0 | 0 | 0 | 1 | Common NFT, has message, has FT
0xA0 | 1 | 0 | 1 | 0 | 0 | 0 | 0 | 0 | Editable NFT, no message, no FT
0xA1 | 1 | 0 | 1 | 0 | 0 | 0 | 0 | 1 | Editable NFT, no message, has FT
0xB0 | 1 | 0 | 1 | 1 | 0 | 0 | 0 | 0 | Editable NFT, has message, no FT
0xB1 | 1 | 0 | 1 | 1 | 0 | 0 | 0 | 1 | Editable NFT, has message, has FT
0xC0 | 1 | 1 | 0 | 0 | 0 | 0 | 0 | 0 | Cloneable NFT, no message, no FT
0xC1 | 1 | 1 | 0 | 0 | 0 | 0 | 0 | 1 | Cloneable NFT, no message, has FT
0xD0 | 1 | 1 | 0 | 1 | 0 | 0 | 0 | 0 | Cloneable NFT, has message, no FT
0xD1 | 1 | 1 | 0 | 1 | 0 | 0 | 0 | 1 | Cloneable NFT, has message, has FT
0xE0 | 1 | 1 | 1 | 0 | 0 | 0 | 0 | 0 | Editable and cloneable NFT, no message, no FT
0xE1 | 1 | 1 | 1 | 0 | 0 | 0 | 0 | 1 | Editable and cloneable NFT, no message, has FT
0xF0 | 1 | 1 | 1 | 1 | 0 | 0 | 0 | 0 | Editable and cloneable NFT, has message, no FT
0xF1 | 1 | 1 | 1 | 1 | 0 | 0 | 0 | 1 | Editable and cloneable NFT, has message, has FT

All other values are reserved and reading a reserved value will immediately fail the transaction.

### Group Logic Consensus Rules

Existing Bitcoin Cash rules will apply the same to both pure and grouped outputs.
Group logic consensus rules are an *additional* set of rules exclusive to grouped outputs.

Group membership and group genesis rules will apply to ALL transactions involving grouped inputs and outputs.
They're sufficient to enable **unforgeable groups** and **unforgeable contracts** features.

On top of that, we will introduce two essential and independent token "super-contracts": non-fungible tokens and fungible tokens.
They will allow applications to set additional constraints over descendants of the grouped output, in effect enabling **native token primitives** that can be extended using Script VM to build any imaginable token system.

#### Group Membership

New outputs of some `group_id` may be created in a transaction if:

- the transaction spends at least one prevout with the same `group_id`, or
- the transaction spends any prevout as a group genesis input such that its newly generated `group_id` matches the output's.

This also means that an input could be adding 2 tokens into the transaction's input-side balance: the pre-existing token attached to the prevout, and the newly created token generated by the input's group genesis annotation.
We don't unnecessarily restrict this, because a Script contract could easily control it, and even make good use of it for constructions such as transitions between distinct groups.

For each group genesis input, at least one output with a matching group ID must be created, i.e. an input can't declare a group initialization and then not have it created.
Group initial state is created by recording the declared state in the UTXO state, by creating first outputs, a genesis input can be thought as only authorizing it.
This is consistent with coinbase *inputs*, they only authorize a change in the UTXO state, but the state is actually changed by creating the coinbase *output*.

New outputs can only be added to the group by inheriting membership from existing member outputs or from the genesis input which will be the first member of any group.

#### Group Genesis

The genesis consensus rule will be the only possible way to create a new group.
Creation of new groups will be authorized by group genesis inputs, which will be explicitly declared using the `PFX_I_GROUP_GENESIS` input field.

Consensus code will deterministically generate 32 bytes for the `group_id` and allow creation of outputs with the matching `group_id` provided they balance against the genesis input.

There can be multiple such inputs in a transaction and two  modes of generating the ID are proposed, determined by the group genesis hash type byte:

1. Hash type `0x00` may only be used if the input is spending a prevout with prevout index 0 in which case the new group's ID will be set to the prevout's TXID.
2. Hash type `0x01` may be used with any input that is not a coinbase input, in which case the new group's ID will be a double SHA-256 hash of byte concatenation of the prevout's TXID and prevout index.

Usage of group genesis input annotation with coinbase inputs will NOT be allowed.
Note that it would be possible to allow it by introducing one more hash type that would indicate use of the previous block hash as group ID.
However, this has no clear benefit and would add to activation costs.
Disallowing it now doesn't preclude some future CHIP from demonstrating a need for it and proposing to extend genesis rules defined here.

Mode 1. allows for a convenience where a token's genesis TX can be looked up by the `group_ID` which may be useful for some applications, but limited in that only a subset of UTXOs  may be used for the genesis input.

Mode 2. allows spending any UTXO for the genesis input, allowing full flexibility in designing more complex applications.

With such definition, a genesis transaction is self-evidently valid or invalid and not depending on non-local data.
A new group is created simply by spending an input with the group genesis annotation and it will allow creation of outputs that match the generated `group_id` and balance against the input.

The genesis declaration isn't signed, but the membership rule requires at least one related output to be created and the outputs [will be signed](#transaction-signing).
Changing anything in the genesis declaration would orphan the output(s) and the transaction would fail.
Using group genesis introspection, contracts can validate a particular setup without having to depend on signatures.
This is discussed in detail under [group genesis malleability section](#group-genesis-malleability).

The `PFX_GROUP_GENESIS` input declaration can be used even if the input is spending some existing group's prevout.
In that case both token amounts will be added to the temporary TX-local tally.

With this setup we need an introspection opcode to access genesis information, because any genesis input could be carrying information about 2 tokens:

1. Some pre-existing token, carried by the prevout being spent
2. The new token, created in-situ with the genesis input declaration

Introspection opcodes will enable reading either or both, so contract writers will have full control over genesis setups.

#### Group Super-contracts

We will introduce two fundamental "super-contracts" that can be used as primitives to build any imaginable token system.
They're enforced independent of each other, which means that fungible token amounts can freely flow between outputs of the same group, independent of the output's NFT state.

##### Non-fungible Tokens Super-contract

This set of consensus rules will enforce non-fungible token (NFT) logic by defining how NFT UTXO state may evolve from any group's initial NFT state.
The domain of the super-contract is only the NFT state, contained in grouped UTXOs that encode NFTs, specifically:

- NFT existence, encoded using bit 7 (`0x80`) of `token_type`,
- NFT capabilities, encoded using bits 5 and 6 (`0x20` and `0x40`) of `token_type`,
- NFT cardinality, i.e. total number _of_ NFT UTXOs with a common `group ID`,
- NFT message(s), encoded using bit 4 (`0x10`) of `token_type` and `nft message` field.

All 4 states of the capability are defined and used:

- None: If `token_type & 0xE0 == 0x80` then the NFT UTXO being spent may pass its NFT state to only one new NFT output which must inherit the NFT message state of the parent.
- Edit: If `token_type & 0xE0 == 0xA0` then the NFT UTXO being spent may create only one new NFT output which may have its NFT message freely set, edited, or removed.
- Clone: If `token_type & 0xE0 == 0xC0` then the NFT UTXO being spent may increase the group's NFT cardinality by cloning its NFT message state to any number of new NFT outputs in the transaction.
- Mint: If `token_type & 0xE0 == 0xE0` then the NFT UTXO being spent is free to create any number of new NFT outputs in the transaction, and freely set, edit, or remove the message on each of them.

New NFT outputs of some capability must inherit it in full from an existing UTXO spent in the same transaction or group genesis input in the same transaction.
Capability may be freely demoted, but it may not be accumulated from multiple parents, i.e. spending an edit-capable and clone-capable NFT won't allow creation of a mint-capable NFT.

Any transaction input may implicitly destroy an NFT by not carrying its state over to newly created NFT output(s) of the transaction.
This means that spending a mint-capable NFT input may be used to effectively edit lesser-capable NFTs of the same group, because in a same transaction they can be destroyed and recreated as descendants of the minting input.

If a group is created without some NFT capability, then ALL NFTs of the group will be permanently without the capability and without the possibility of obtaining it later.
Sometimes this will be desireable, and sometimes capability will be preserved but a Script covenant will be placed on the NFT to limit the use.

This allows for creation of any imaginable blockchain structure, and even implementing fungible tokens using Script, where they could use the NFT message field for their amount state.
Because fungible tokens are essential building blocks of blockchain applications, we propose to introduce them as the only other super-contract.

##### Fungible Tokens

This set of rules will enforce fungible token (FT) logic by defining how FT UTXO state may evolve from any group's initial FT state.
The `fungible token amount` field is dedicated for this super-contract and enforcement isn't opt-in as with the NFT's message field.
The domain of the super-contract is only the FT state, contained in grouped UTXOs that encode FTs, specifically:

- FT existence, encoded using bit 0 (`0x01`) of `token_type`,
- FT amount, encoded using `fungible token amount` field.

The super-contract will enforce input and output sums of the respective `group_amount` field to balance across a transaction.
The entire supply is created with the genesis input, and later it may only be reduced by omitting amounts from the output side.
In other words, for each distinct group in a transaction the sum of amounts in must be less than or equal than the sum of amounts out.

Group amount is allowed to be 0, which is consistent with BCH consensus rules and it will be recommended to also restrict them to grouped `OP_RETURN` outputs by using network rules.

### Group Annotation Introspection

We will extend [CHIP-2021-02: Native Introspection Opcodes](https://gitlab.com/GeneralProtocols/research/chips/-/blob/master/CHIP-2021-02-Add-Native-Introspection-Opcodes.md) with 3 **binary** opcodes, one for each context in which a group state may appear: `OP_INPUTPFXGROUP`, `OP_UTXOPFXGROUP`, and `OP_OUTPUTPFXGROUP`.
Each will pop two stack items as Script numbers: an input or output index, and the PreFiX field index.
If so referenced field exists then they will push its value to the stack, else push an empty stack element.

There are 4 values that can be returned for each context, leading to total 12 possible calls:

- `<0> <0> OP_INPUTPFXGROUP` returns first transaction input's `group genesis hash type`.
- `<0> <1> OP_INPUTPFXGROUP` returns first transaction input's `initial token type`.
- `<0> <2> OP_INPUTPFXGROUP` returns first transaction input's `initial fungible token amount`.
- `<0> <3> OP_INPUTPFXGROUP` returns first transaction input's `initial nft message`.
- `<0> <0> OP_UTXOPFXGROUP` returns first transaction input UTXO's `group ID`.
- `<0> <1> OP_UTXOPFXGROUP` returns first transaction input UTXO's `token type`.
- `<0> <2> OP_UTXOPFXGROUP` returns first transaction input UTXO's `fungible token amount`.
- `<0> <3> OP_UTXOPFXGROUP` returns first transaction input UTXO's `nft message`.
- `<0> <0> OP_OUTPUTPFXGROUP` returns first transaction output's `group ID`.
- `<0> <1> OP_OUTPUTPFXGROUP` returns first transaction output's `token type`.
- `<0> <2> OP_OUTPUTPFXGROUP` returns first transaction output's `fungible token amount`.
- `<0> <3> OP_OUTPUTPFXGROUP` returns first transaction output's `nft message`.

Note that raw transaction format uses the "compact-size uint" number serialization to encode numbers in a more compact form which is incompatible with Script VM.
Introspection opcodes will be specified so that numbers are always pushed to Script VM stack in their deserialized form, so that Script VM may directly use the returned value instead of having to extract it from the encapsulation.

Specifying group annotation introspection as 3 binary opcodes is a compromise between contract byte-efficiency and using too many of the remaining and scarce Script VM code-points.
The `OP_INPUTPFXGROUP` will be specified at the same code-point (`0xEE`) as `PFX_GROUP_GENESIS` and `PFX_GROUP`, completing the definition across all 3 contexts.

With the above, this proposal will use up a total of 3 code-points.

### Transaction Signing

In order to prevent transactions involving grouped outputs from being malleable, output's group annotation must be included in Bitcoin Cash signature [preimage format](https://reference.cash/protocol/blockchain/transaction/transaction-signing#preimage-format).

New `SIGHASH_GROUP` bit will be defined to indicate signature preimage format modification, and any signature that contains a grouped prevout or output will require use of the bit.
Both newly created outputs and prevouts being spent will have their group annotation included inside the signature preimage.
If present, input's group genesis annotation shall be included as well.
This way:

- Accidentally burning tokens by non-upgraded wallets will be prevented;
- Transactions won't be malleable;
- Offline signers will have access to prevout's token information.

### Network Rules

The standard locking script rules need to be updated to recognize grouped output format.
Because grouped UTXOs will be bigger, another dust limit will be defined for grouped outputs.
Treatment of data carrier and 0-value fungible tokens will be defined to be consistent with Bitcoin Cash rules.

### Wallet Address Type

Because we require transactions involving grouped outputs to be signed using a new signature hash type bit, funds unintentionally sent to non-upgraded wallets can't be accidentally lost and will be recoverable by importing the recipient's wallet into upgraded software.

The existing [CashAddress](https://github.com/bitcoincashorg/bitcoincash.org/blob/master/spec/cashaddr.md#payload) standard supports extending the address through the use of a type bit, which could be used to indicate wallet support for receiving tokens:

>Encoding the size of the hash in the version field ensure that it is possible to check that the length of the address is correct.
>
>| Type bits |      Meaning      | Version byte value |
>| --------: | :---------------: | -----------------: |
>|         0 |       P2KH        |                  0 |
>|         1 |       P2SH        |                  8 |
>
>Further types will be added as new features are added.

However we expect that, over time, most wallets will support receiving and sending tokens.
Introducing two more address types just to signal readiness to receive tokens would only have a temporary benefit, while the cost of having to maintain two more address encodings would be permanent.

Therefore, we do NOT recommend a new address type.

## Specification

[[Back to Contents](#contents)]

The [technical description](#technical-description) section is intended to explain design choices and mechanics of rules presented here.
This section defines formal rules for validating transactions  that involve grouped inputs and outputs and is intended for implementers.

### Transaction Format Changes

**Definitions**

We define transaction input **P**re**F**i**X** byte that will indicate presence of optional **group genesis input annotation**:

- `PFX_I_GROUP_GENESIS = 0xEE`.

We define transaction output **P**re**F**i**X** byte that will indicate presence of optional **group output annotation**:

- `PFX_O_GROUP = 0xEE`.

Note that we can define them using the same byte `0xEE` because the two prefixes will be used in distinct contexts.

We define bit flags that will indicate presence of optional group annotation fields:

- `HAS_FT_AMOUNT = 0x01`,
- `HAS_NFT_MESSAGE = 0x10`.

We define the maximum length for NFT messages:

- `MAX_NFT_MESSAGE_LENGTH = 40`.

#### Transaction Inputs

If `PFX_I_GROUP_GENESIS` byte is encoded at index 0 of a transaction input's unlocking bytecode then it will mark the start of a data structure that will encode the group genesis input annotation.
The whole annotation, including the PFX byte, will be removed from the unlocking bytecode before being handed to Script VM for evaluation:

*`<prevout hash><prevout index><unlocking script length>`* **`[PFX_I_GROUP_GENESIS <fields>]`** *`<real unlocking script><sequence number>`*.

Group genesis input annotation is defined as:

`PFX_I_GROUP_GENESIS <group_genesis_hash_type> <initial_token_type> [initial_ft_amount] [<initial_nft_message_length><initial_nft_message>]`

where:

- `group_genesis_hash_type` - a 1-byte value
- `initial_token_type` - a 1-byte value, where bits 0 and 4 will indicate presence of optional fields
- `initial_ft_amount` - a compact-size uint value that may encode a 1, 2, 4, or 8 byte uint, encoded only if `initial_token_type & HAS_FT_AMOUNT == HAS_FT_AMOUNT`
- `initial_nft_message_length` - a compact-size uint value that may encode a 1, 2, 4, or 8 byte uint, encoded only if `initial_token_type & HAS_NFT_MESSAGE == HAS_NFT_MESSAGE`
- `initial_nft_message` - array of `initial_nft_message_length` bytes, encoded only if `initial_token_type & HAS_NFT_MESSAGE == HAS_NFT_MESSAGE`

Value of `initial_ft_amount` must obey these limits: minimum value `0` and maximum value `9223372036854775807` (`0x7fffffffffffffff`). (REQ-X)

Value of `initial_nft_message_length` must obey these limits: minimum value `0` and maximum value equal to lower of [`MAX_SCRIPT_ELEMENT_SIZE`](https://gitlab.com/bitcoin-cash-node/bitcoin-cash-node/-/blob/master/src/script/script.h#L23) and `MAX_NFT_MESSAGE_LENGTH` defined above. (REQ-X)

If `HAS_NFT_MESSAGE` is set and `initial_nft_message_length == 0` then the `initial_nft_message` will be an empty array.
This is intentional and means that NULL and EMPTY are distinct states of the NFT message.

The annotation SHALL NOT count against length limits placed on `unlocking script`. (REQ-X)

#### Transaction Outputs

If `PFX_O_GROUP` byte is encoded at index 0 of a transaction outputs's locking bytecode then it will mark the start of a data structure that will encode the group output annotation.
The whole annotation, including the PFX byte, will be removed from the locking bytecode before evaluation by Script VM:

*`<satoshi value><unlocking script length>`* **`[PFX_O_GROUP <fields>]`** *`<real locking script>`*.

A single input may only include one such annotation.

Group output annotation is defined as:

`PFX_O_GROUP <group_ID> <token_type> [ft_amount] [<nft_message_length><nft_message>]`

where:

- `group_ID` - a 32-byte value
- `token_type` - a 1-byte value, where bits 0 and 4 will indicate presence of optional fields
- `ft_amount` - a compact-size uint value that may encode a 1, 2, 4, or 8 byte uint, encoded only if `token_type & HAS_FT_AMOUNT == HAS_FT_AMOUNT`
- `nft_message_length` - a compact-size uint value that may encode a 1, 2, 4, or 8 byte uint, encoded only if `token_type & HAS_NFT_MESSAGE == HAS_NFT_MESSAGE`
- `nft_message` - array of `nft_message_length` bytes, encoded only if `token_type & HAS_NFT_MESSAGE == HAS_NFT_MESSAGE`

Value of `ft_amount` must obey these limits: minimum value `0` and maximum value `9223372036854775807` (`0x7fffffffffffffff`). (REQ-X)

Value of `nft_message_length` must obey these limits: minimum value `0` and maximum value equal to lower of [`MAX_SCRIPT_ELEMENT_SIZE`](https://gitlab.com/bitcoin-cash-node/bitcoin-cash-node/-/blob/master/src/script/script.h#L23) and `MAX_NFT_MESSAGE_LENGTH` defined above. (REQ-X)

If `HAS_NFT_MESSAGE` is set and `nft_message_length == 0` then the `nft_message` will be an empty array.
This is intentional and means that NULL and EMPTY are distinct states of the NFT message.

The annotation SHALL NOT count against length limits placed on `locking script`. (REQ-X)

### Transaction Validation Changes

These consensus rules will be enforced over every transaction that involves grouped outputs.
The enforcement is transaction-local, i.e. everything required to validate the transaction is contained in the transaction and the prevouts (UTXOs) it references.

Token Type and Group Genesis rules will be enforced for individual inputs and outputs that encode a group annotation, while Group Membership, Fungible Tokens and Non-fungible Tokens rules will be enforced for aggregates of grouped inputs and outputs in the transaction.

#### Token Type

The `token_type` shall declare usage of optional fields as defined above, and also encode an NFT state.

Combined with transaction format definition, here we will define all flags:

- `HAS_FT_AMOUNT = 0x01`
- `RESERVED_TYPE_FLAGS = 0x0E`
- `HAS_NFT_MESSAGE = 0x10`
- `NFT_EDITABLE = 0x20`
- `NFT_CLONEABLE = 0x40`
- `HAS_NFT_STATE = 0x80`

To make it easier to define other rules, we will also define:

- `NFT_CAPABILITIES = NFT_EDITABLE | NFT_CLONEABLE`
- `NFT_STATE = NFT_CAPABILITIES | HAS_NFT_MESSAGE`

If any grouped genesis input or grouped output satisfies `token_type & RESERVED_TYPE_FLAGS != 0` the transaction shall fail immediately. (REQ-X)

The grouped genesis input or grouped output may encode an NFT state only if the `HAS_NFT_STATE` flag is set meaning that every grouped genesis input or grouped output MUST satisfy: `(token_type & NFT_STATE & HAS_NFT_STATE == 0) || (token_type & HAS_NFT_STATE != 0)`. (REQ-X)

#### Group Consensus Logic

##### Group Genesis

Group genesis inputs don't have their `group_ID` recorded but are instead each associated with a calculated value `group_ID`, defined as follows:

1. If `group_genesis_hash_type == 0x00` then `group_ID = prevout.txid`.
2. If `group_genesis_hash_type == 0x01` then `group_ID = Hash(prevout.txid || prevout.index)`.

where `Hash` is the double SHA-256 function and `||` indicates byte concatenation.

Other values are reserved.
The value of `group_genesis_hash_type` MUST be either `0x00` or `0x01`. (REQ-X)

Coinbase transactions MUST NOT use the genesis input prefix. (REQ-X)

Inputs that use `group_genesis_hash_type == 0x00` MUST also have their `prevout.index == 0`. (REQ-X)

Inputs that spend a prevout with vout index 0 MAY use either value for the `group_genesis_hash_type` and have the `group_ID` computed using the mode indicated. (REQ-X)

Any input MAY carry 2 distinct token states: that of the input's prevout token state and that of the group genesis initial token state. (REQ-X)

Aggregates (sums and counts) computed for the purpose of validating transaction balancing rules will include both token states: the pre-existing token state inherited from the prevout and the newly created token state, created by the input.

##### Group Membership

For each distinct `group_ID` observed in transaction's outputs there MUST exist a prevout with a matching `group_ID` or a group genesis input with a matching `genesis_group_ID`. (REQ-X)

For each distinct `group_ID` observed in transaction's group genesis inputs there MUST exist at least one output with a matching `group_ID`. (REQ-X)

##### Fungible Tokens

For each distinct `group_ID` observed in a transaction, sum of `ft_amount` values on the output side MUST not exceed the maximum value of 9223372036854775807 (0x7fffffffffffffff). (REQ-X)

For each distinct `group_ID` observed in a transaction, sum of `ft_amount` values on the input side MUST not exceed the maximum value of 9223372036854775807 (0x7fffffffffffffff). (REQ-X)

For each distinct `group_ID` observed in a transaction, sum of `ft_amount` on the output side MUST be less than or equal than the sum of `ft_amount` on the input side. (REQ-X)

For each distinct `group_ID` observed in a transaction, if there's at least one output that encodes the `ft_amount` field then there MUST exist at least one input that encodes the  `ft_amount`. (REQ-X)

This prevents NFT-only inputs from emitting 0-value fungible token outputs.

Note that this allows creating fungible tokens with 0 supply and any emitting any number of grouped outputs with 0 amount.
This is consistent with existing BCH consensus rules.

##### Non-fungible Tokens

For each distinct `group_ID` observed in a transaction, if there exists an output with both NFT_EDITABLE and NFT_CLONEABLE flags set then there must exist at least one input with both of those same flags set. (REQ-X)

For each distinct `group_ID || NFTHasMessage() || nft_message` observed in a transaction, if there exists an output with NFT_EDITABLE flag NOT set and NFT_CLONEABLE flag set then there must exist at least one input with the NFT_CLONABLE flag set, unless there exits an input of the same `group_ID` with both flags set. (REQ-X)

For each distinct `group_ID`, the count of outputs with the NFT_EDITABLE flag set must be less than or equal than the count of inputs with the NFT_EDITABLE flag set, unless there exits an input with both NFT_EDITABLE and NFT_CLONEABLE flags set. (REQ-X)

We define a `nft_edit_budget` as a difference between those two counts, and only if there's more such inputs then outputs.

We define a `nft_unmatched` as a total of differences between count of inputs and outputs for each distinct `group_ID || NFTHasMessage() || nft_message` observed in a transaction that have both flags NOT set.
The difference is added only for cases where there's more outputs than inputs, and there doesn't exist an input with the same message and NFT_CLONEABLE  flag set.

The aggregate `nft_unmatched` MUST be less than or equal than `nft_edit_budget`, unless there exits an input of the same `group_ID` with both flags set. (REQ-X)

#### Pay-to-Script-Hash (P2SH)

When combined with the group annotation, both current 20-byte P2SH and simultaneously proposed 32-byte P2SH output patterns MUST activate BIP-0016 redeem script consensus logic all the same:

- P2SH20: `PFX_O_GROUP group_state OP_HASH160 <hash_20> OP_EQUAL`
- P2SH32: `PFX_O_GROUP group_state OP_HASH256 <hash_32> OP_EQUAL`

(REQ-X)

### Script Virtual Machine Changes

Note that the code point `0xEE` will be repurposed and used in 3 contexts:

- Group genesis input annotation, labeled `PFX_I_GROUP_GENESIS`;
- Group genesis input annotation introspection Script VM opcode, labeled `OP_INPUTGROUPGENESIS`, which will be defined below;
- Group output annotation, labeled `PFX_O_GROUP`.

This is possible because PFX annotations get snipped off from their respective script before handing it to Script VM and the `0xEE` byte was a disabled opcode.

#### Group Annotation Introspection Opcodes

We will extend [CHIP-2021-02: Native Introspection Opcodes](https://gitlab.com/GeneralProtocols/research/chips/-/blob/master/CHIP-2021-02-Add-Native-Introspection-Opcodes.md) with 3 binary opcodes for reading group annotation:

- `OP_UTXOPFXGROUP == 0xED` Pop the top item from the stack as an inner index (Script Number).
Pop the next top item from the stack as an input index (Script Number). 
From that input, push to the stack the value of so referenced input's prevout group annotation field.
Inner index refers to:
    - 0x00 - `group_id`, pushed as a 32-byte array in `OP_HASH256` byte order
    - 0x01 - `token_type`, pushed as a single byte
    - 0x02 - `fungible_token_amount`, pushed as a script number
    - 0x03 - `nft_message`, pushed as a byte array
- `OP_INPUTPFXGROUP == 0xEE` Pop the top item from the stack as an inner index (Script Number).
Pop the next top item from the stack as an input index (Script Number). 
From that input, push to the stack the value of so referenced group genesis annotation field.
Inner index refers to:
    - 0x00 - `group_genesis_hash_type`, pushed as a single byte
    - 0x01 - `initial_token_type`, pushed as a single byte
    - 0x02 - `initial_fungible_token_amount`, pushed as a script number
    - 0x03 - `initial_nft_message`, pushed as a byte array
- `OP_OUTPUTPFXGROUP == 0xEF` Pop the top item from the stack as an inner index (Script Number).
Pop the next top item from the stack as an output index (Script Number). 
From that output, push to the stack the value of so referenced output's group annotation field.
Inner index refers to:
    - 0x00 - `group_id`, pushed as a 32-byte array in `OP_HASH256` byte order
    - 0x01 - `token_type`, pushed as a single byte
    - 0x02 - `fungible_token_amount`, pushed as a script number
    - 0x03 - `nft_message`, pushed as a byte array

If an optional field is referenced but isn't used, return an empty stack item.
If the prevout, input, or output doesn't encode a group annotation at all, return an empty stack item.

#### Signature Changes

##### SIGHASH_TOKEN

Any signature that includes a grouped prevout, input or output MUST have its bit 5 set on its `nHashType` - this is here defined as `SIGHASH_GROUP`, `0x20`. (REQ-X)

Any signeture that does not include any grouped prevouts, inputs or outputs MUST NOT have the `SIGHASH_GROUP` bit set. (REQ-X)

When combined with existing sighash types, taking `SIGHASH_FORKID` (0x40) into account, this then becomes: 

| Flag | Value for non-token signature, Hex / Binary | Value for token signature, Hex / Binary |
| ----------------------------- | ---------------- | ---------------- |
| SIGHASH_ALL                   | 0x41 / 0100 0001 | 0x61 / 0110 0001 |
| SIGHASH_NONE                  | 0x42 / 0100 0010 | 0x62 / 0110 0010 |
| SIGHASH_SINGLE                | 0x43 / 0100 0011 | 0x63 / 0110 0011 |
| SIGHASH_ALL / ANYONECANPAY    | 0xC1 / 1100 0001 | 0xE1 / 1110 0001 |
| SIGHASH_NONE / ANYONECANPAY   | 0xC2 / 1100 0010 | 0xE2 / 1110 0010 |
| SIGHASH_SINGLE / ANYONECANPAY | 0xC3 / 1100 0011 | 0xE3 / 1110 0011 |

##### Signature Preimage

[Preimage format](https://reference.cash/protocol/blockchain/transaction/transaction-signing#preimage-format) will remain the same.

[Transaction outputs](https://reference.cash/protocol/blockchain/transaction/transaction-signing#transaction-outputs-hash) hash shall include the whole group output annotation as if part of the `lockingScript`. (REQ-X)

[Prevout modified locking script](https://reference.cash/protocol/blockchain/transaction/transaction-signing#modified-locking-script) hash shall include the whole group output annotation as a prefix to the modified script described in current specification. (REQ-X)

Group genesis input annotations will not be part of signature preimages.

### Network Rules

#### Standard Locking Scripts

It is required to update the [standard locking script](https://reference.cash/protocol/blockchain/transaction/locking-script) rules to recognize existing locking script patterns combined with group annotation.

We will summarize standard outputs below:

- P2PK (deprecated): `<key> OP_CHECKSIG`
- P2PKH: `OP_DUP OP_HASH160 <hash_20> OP_EQUALVERIFY OP_CHECKSIG`
- P2MS: `<m><<key 1>...<key n>><n> OP_CHECKMULTISIG`
- OP_RETURN: `OP_RETURN [data 0]...[data N]`
- P2SH20: `OP_HASH160 <hash_20> OP_EQUAL`
- P2SH32 (proposed for 2023): `OP_HASH256 <hash_32> OP_EQUAL`

Standard locking script rules MUST allow those templates combined with group annotation:

- P2PK (deprecated): `PFX_O_GROUP group_state <key> OP_CHECKSIG`
- P2PKH: `PFX_O_GROUP group_state OP_DUP OP_HASH160 <hash_20> OP_EQUALVERIFY OP_CHECKSIG`
- OP_RETURN: `PFX_O_GROUP group_state OP_RETURN [data 0]...[data N]`
- P2SH20: `PFX_O_GROUP group_state OP_HASH160 <hash_20> OP_EQUAL`
- P2SH32: `PFX_O_GROUP group_state OP_HASH256 <hash_32> OP_EQUAL`

(REQ-X)

Depending on implementation, the above rules may not need an upgrade at all because the PFX annotation can be removed from the `locking script` and only the `real locking script` be pattern-matched against standard templates.

#### Dust Limit

If the output encodes the group annotation and the output is not a data carrier output then value of `satoshi_amount` MUST be greater than `GROUP_DUST_LIMIT`, here defined as `GROUP_DUST_LIMIT = 795`. (REQ-X)

#### Zero Amounts

If the output encodes the `ft_amount` field and the output is not a data carrier output then the `ft_amount` value MUST NOT be 0. (REQ-X)

#### Data Carrier Outputs (OP_RETURN)

If a data carrier output encodes a `ft_amount` field then the `ft_amount` value MAY be 0. (REQ-X)

## Security Analysis

[[Back to Contents](#contents)]

### Group Genesis Soundness

Soundness of the proposed system relies on assumption that the `group_ID` set on genesis transaction may be used only by outputs that have that transaction as an ancestor.
It must not be possible to have another genesis transaction mined that would result in the same `group_ID` as some previously-mined genesis transaction.
This property is guaranteed the same way uniqueness of TXIDs and block hashes is guaranteed.

Using previous block hash and prevout TXID for the `group_ID` ensures that token genesis input's generated `group_ID` will be unforgeable and unique.
In other words, for any `group_ID` there can only ever exist exactly one genesis input.
It is so because a duplicate group genesis TX would be rejected for double-spending the same output, or for being part of a wrong block in case of coinbase input group genesis.
Crafting 2 transactions using distinct outputs while re-rolling other parts of `group_ID` preimages to attempt to find a hash collision is computationally impossible because it would take 2^(256/2) attempts.
In other words, Wagner's birthday attack on `group_ID` is impossible because it will have 128 bits of security against such attack, same as block hashes and TXIDs, so `groupID` will be as secure as those.

Note that the risk of a birthday attack is not the same as with Bitcoin Cash addresses where collisions are feasible with the 160-bit address format.
A colliding address pair will have the same owner and is generally not a problem.
A colliding `group_ID` could be used to post another genesis TX after it's already been created for that `group_ID`, which would destroy confidence in the whole system by disproving the unforgeability conjecture.

### Group Genesis Malleability

The group genesis annotation will not be signed by current signature algorithms.
At first look, it may seem that it would open a transaction malleability vector:

1. Construct a group genesis transaction.
2. Sign using SIGHASH_ALL.
3. Broadcast.
4. Attacker modifies PFX_GROUP_GENESIS arguments.
5. TXID is changed, and signature still valid.
6. Modified transaction is accepted to the blockchain.

This is not a viable attack and it would fail at 6. because:

- Signature would indeed be valid.
- Transaction itself would be invalidated for violating group consensus rules:
    - Any genesis `group_ID` generated by the input declaration MUST appear in at least one output.
    - Any `group_ID` seen on outputs MUST appear in at least one input (either ordinary or genesis).

In other words, removing the group genesis annotation or changing any part of it would either remove the `group_ID` from the input or change it to some other one, not present on outputs.
This would orphan signed outputs, and orphans are not allowed by group consensus rules.

Because outputs are signed, attacker can't update the related `group_ID` on the outputs to make the transaction valid, as that would invalidate the signature since outputs, including their group annotation, are part of the signature preimage.
Also note that changing the genesis input index would invalidate the signature (SIGHASH_ALL).

When using P2SH, the genesis annotation can be locked by using introspection opcodes to verify genesis parameters against expected ones.

### Immutable Group Properties

They're guaranteed by induction, similarly to BCH properties.
By consistently using consensus to enforce group rules for each transaction, below listed group properties will be globally immutable:

- Single DAG root (genesis input) for the group;
- Every group member will carry a partial proof of its genesis;
- Accounting equation over the `groupAmount` will hold globally for all fungible tokens of the group.
- NFT messages are guaranteed to have come from either the group's genesis TX or an edit-capable NFT spend.
- If genesis input declared an NFT without cloneable capability, it will be guaranteed that there can only ever exist 1 NFT UTXO of the group.

### DoS Vectors

#### Group Genesis

Group genesis verification requires a hash operation only if `group_genesis_hash_type == 0x02`.
In other cases, it uses an already computed value (TXID or previous block hash).
The number of genesis inputs is not limited, so could posting a TX filled with genesis inputs slow down transaction validation and be used as a DoS vector?

The data to be hashed is rigidly defined (TXID || index) and doesn't introduce a DoS vector considering many more hash operations can already be executed by locking and redeem scripts.

#### Transaction Local Index Size

Validating group consensus rules will require some transaction-local temporary indexes to be built in memory.
The size of the index could grow to a maximum number of inputs or outputs that carry a distinct `group_ID` or `(group_ID, nft_message)`.
Could posting a TX filled with the maximum number of distinct IDs be used as a DoS vector?

Upper bound of the temporary index can be calculated from current consensus limits on transaction size.
Outputs have less overhead and are therefore a cheaper way to inflate the number of possible distinct `groupID`s.
Consensus limits transaction size to 1MB, and the smallest input is an anyone-can-spend with OP_1 as the signature, which would be 42 bytes.
Smallest output would be an amountless group type with an `OP_RETURN` output script, which would be 41 bytes.
From that we can calculate the maximum number of distinct `groupID`s in a transaction:
`(1000000-42)/41 == 24389`.
Index building operations are of `O(n*log(n))` time complexity, and with the above transaction size limit local index building will be `4.4*n` at worst which is insignificant when compared to the cost of a single signature validation.

Block validation will continue to be O(n).

#### Invalid Transactions

At worst, detecting an invalid transaction will require processing the whole 1MB transaction after which the TXID can be banned if found invalid.

This is the same as current situation.

Grouped transactions would reduce density of TX CPU operations because each grouped output has 41 bytes of "dumb" data.
If an attacker wanted to to load node CPUs, he could pack the 41 bytes with some expensive opcodes instead.

Implementations must guard against the possibility of invalid transactions causing undefined behavior, such as attempting to overflow tokens sums during accumulation, before consensus validation gets to verifying the sums.

### Consensus Failure

The proposal specification can be implemented with the structures and flow of existing codebases i.e. it doesn't require implementing some new untested hashing algorithm, serialization function or similar.

Implementation will rely on:

- Raw bytes manipulation (slicing, concatenation);
- Bitwise operations;
- Local cache index building (in-memory maps and vectors, in C++ terms);
- SHA-256 function;
- Arithmetic operations.

Experienced node developers should have no trouble guarding against textbook "gotchas" such as endianness mismatch, overflows, off-by-one, etc.

## Implementations

[[Back to Contents](#contents)]

- [{WIP} BCHN](https://gitlab.com/bitcoin-cash-node/bitcoin-cash-node/-/merge_requests/1203)

For reference, original implementation with a broader set of features:

- [BU implementation](https://gitlab.com/gandrewstone/BCHUnlimited/-/blob/0d8b59d47e954a6bf40e5a031129924f5edd1914/src/consensus/grouptokens.cpp)
- [BU demo](https://www.nextchain.cash/groupTokensCliExample)

## Test Cases

[[Back to Contents](#contents)]

{TODO}

For reference, tests for the original implementation:

- [BU/grouptoken_tests.cpp](https://gitlab.com/gandrewstone/BCHUnlimited/-/blob/0d8b59d47e954a6bf40e5a031129924f5edd1914/src/test/grouptoken_tests.cpp)
- [BU/grouptokens.py](https://gitlab.com/gandrewstone/BCHUnlimited/-/blob/0d8b59d47e954a6bf40e5a031129924f5edd1914/qa/rpc-tests/grouptokens.py)

## Activation Costs

[[Back to Contents](#contents)]

We define activation cost as any cost that would arise ex-ante, in anticipation of the activation by affected stakeholders, to ensure that their services will continue to function without interruption once the activation block will be mined.
In case of this proposal, activation cost is contained to nodes and will amount to:

- Some fixed amount of node developer man-hours, in order to release updated node software that will correctly validate transactions implementing the new consensus specification.
- Some fixed amount of work by stakeholders running the node software, in order to deploy the software in anticipation of hard-fork activation.
- Some fixed amount of effort by others involved in reviewing and testing the new node software version.

This is the same kind of cost that any hard-fork upgrade enabling previously disabled opcodes has had or will have.
Nothing is required of stakeholders who are not running validating nodes or involved in node development.

Below we will discuss why this is the case and discuss potential impact in more detail, on both nodes and node-dependent software, and for that we will split activation cost the following way:

1. Transaction Format
2. Script Interpreter
3. Node APIs
4. Network Rules
5. Group Consensus Logic

### Transaction Format

The upgrade is extending transaction outputs with additional output fields using the non-breaking method proposed [here](https://gitlab.com/bitcoin-cash-node/announcements/-/blob/master/2021-12-23_evaluate_viability_of_transaction_format_or_id_change_EN.md#an-alternative-to-breaking-change).

If nodes just want to be able to validate transactions then they do not need to change their local database structures.
Group annotation can continue to live inside the existing UTXO database as if it was part of the output's `lockingScript`.
Existing databases must already be supporting longer `lockingScript` values than the standard templates because current consensus rules allow for expanded locking scripts instead of just the compressed P2SH variant.

Implementers may decide to restructure UTXO database structure(s) by segregating the two new fields, but that is not necessary just to be able to validate transactions and be ready for upgrade activation.

### Script Interpreter

#### Script PreFiX on Outputs

Upgraded node software will snip off the `PFX_GROUP` and its arguments from the `lockingScript` and pass the remainder to the Script interpreter.
The node Script interpreter will never get to see the Group annotation, and, from the point of view of Script interpreter, the `PFX_GROUP` byte (`0xEE`) will continue to be a disabled opcode.
Node APIs can be upgraded in a backwards-compatible way by hiding the PFX annotation from responses to queries that would be asking for the `lockingScript`.
The only place where this can't be done is the "raw" transaction query which is the single source of truth about a transaction.

Node-dependent software that would alone extract the `lockingScript` from the raw format would get to see the Group annotation as if it was part of outputs locking script.
In the case of services like indexers, block explorers, etc. this is not a problem as they don't interpret the Script, they simply report what's stored in the field.

Software that would interpret the `lockingScript` would see that it starts with a disabled opcode and would consider the transaction invalid, so that software would need to be upgraded.
Wallets that use simple heuristic to detect outputs that belong to them may ignore grouped outputs or they may include their BCH balances but they wouldn't know how to spend the outputs found.

This is the same kind of cost as enabling a new opcode but it would affect only a subset of software, only the software that would get to see the PFX and interpret it as an opcode.

To better illustrate why this is so, consider the [CHIP-2021-02: Native Introspection Opcodes](https://gitlab.com/GeneralProtocols/research/chips/-/blob/master/CHIP-2021-02-Add-Native-Introspection-Opcodes.md), that is currently locked in for activation in May 2022.

The nullary opcode `OP_INPUTINDEX` uses the previously disabled `0xC0` opcode.
Ignoring `isStandard()` network rules, it will allow anyone with some hash-power to mine a transaction with the locking script:

(1) `OP_INPUTINDEX OP_PUSHDATA_39 "39 bytes of random data" OP_2DROP OP_DUP OP_HASH160 <PubkeyHash> OP_EQUALVERIFY OP_CHECKSIG`.

The transaction is really a P2PKH output template prefixed by a no-op pattern.
It is spendable using the standard P2PKH input script: `<Sig> <PubKey>`.
Wallets that would scan for P2PKH pattern anywhere in the `lockingScript` and ignoring the total length of the `lockingScript` field could detect this pattern and be able to spend it.
An unspendable variant is possible, too:

(2) `OP_INPUTINDEX OP_0 OP_PUSHDATA_38 "38 bytes of random data" OP_DROP OP_DUP OP_HASH160 <PubkeyHash> OP_EQUALVERIFY OP_CHECKSIG`.

Here, the prefix pattern would not be a no-op because the prefix would leave a dirty stack, making the transaction unspendable.
Wallets that would scan for P2PKH pattern anywhere in the `lockingScript` and ignoring the total length of the `lockingScript` field could detect this pattern and not be able to spend it, which may prevent the user from spending their other outputs if the wallet would try to spend it alongside others, resulting in wallet downtime until the bug is resolved.
User funds would never be at risk.
This would affect only the wallets that scan for known patterns in this particular way, the way that ignores the position of the pattern and the total length of the `lockingScript` field.

Some group P2PKH output full locking script would look like the following:

(3) `PFX_GROUP "41 bytes of random data" OP_DUP OP_HASH160 <PubkeyHash> OP_EQUALVERIFY OP_CHECKSIG`.

For some unupgraded software dealing with the raw `unlockingScript` field, transactions (2) and (3) would be equivalent in that they are:

- Both starting with a disabled opcode, one with `0xC0`, the other with `0xEE`,
- Both of the same length, 65 bytes,
- Both ending with a known 24-byte P2PKH pattern.

Comparing raw bytes side by side better illustrates this:

- `(1)` `C0 260123456789ABCDEF0123456789ABCDEF0123456789ABCDEF0123456789ABCDEF0123456789ABCD6D 76A9 {PubkeyHash} 88AC`
- `(2)` `C0
00250123456789ABCDEF0123456789ABCDEF0123456789ABCDEF0123456789ABCDEF0123456789AB75 76A9 {PubkeyHash} 88AC`
- `(3)` `EE 0123456789ABCDEF0123456789ABCDEF0123456789ABCDEF0123456789ABCDEF010000000000000000 76A9 {PubkeyHash} 88AC`

All 3 examples could be detected by some wallet scanning for the `76A9 {PubkeyHash} 88AC` suffix, where:

- (1) is spendable,
- (2) is unspendable, and
- (3) would be spendable only by wallets that would know how to construct group transactions.

#### Script PreFiX on Inputs

Upgraded node software will snip off the `PFX_GROUP_GENESIS` and its arguments from the `unlockingScript` and pass the remainder to the Script interpreter.
The node Script interpreter will never get to see the Group annotation, and, from the point of view of Script interpreter, the `PFX_GROUP_GENESIS` byte (`0xEE`) will continue to be a disabled opcode.
Node APIs can be upgraded in a backwards-compatible way by hiding the PFX annotation from responses to queries that would be asking for the `unlockingScript`.
The only place where this can't be done is the "raw" transaction query which is the single source of truth about a transaction.

Node-dependent software that would alone extract the `unlockingScript` from the raw format would get to see the group genesis annotation as if it was part of inputs unlocking script.
In the case of services like indexers, block explorers, etc. this is not a problem as they don't interpret the Script, they simply report what's stored in the field.

Software that would interpret the raw `unlockingScript` would see that it starts with a disabled opcode and would consider the transaction invalid, so that software would need to be upgraded.

This is the same kind of cost as enabling a new opcode but it would affect only a subset of software, only the software that would get to see the PFX and interpret it as an opcode.

#### Introspection Opcodes

These are documented in [CHIP-2021-02: Native Introspection Opcodes](https://gitlab.com/GeneralProtocols/research/chips/-/blob/master/CHIP-2021-02-Add-Native-Introspection-Opcodes.md#costs-risk-mitigation) where introspection opcodes proposed here can be seen as an extension of that upgrade.

#### Signature Opcodes

This proposal modifies only the `lockingScript` part of the signature preimage so:

1. Unupgraded sofware would generate the signature by including the whole `lockingScript` in the preimage, which would include the whole Group annotation.
2. Upgraded software would generate the exact same preimage, even if the PFX annotation is semantically detached from the `lockingScript`.

The `unlockingScript` isn't part of the signature preimage, and we are not adding the genesis annotation so both unupgraded and upgraded software would be generating the same signature preimage.

Unupgraded software would be able to generate or validate signatures of group transactions, even though it wouldn't fully understand what is signed.

### Node APIs

This proposal doesn't specify how node APIs providing transaction and output data are to be upgraded, that is at discretion of each individual node developer team.
All existing node APIs can remain the same to support the upgrade without breaking API-dependent software, in which case the dependent software will be unaware of outputs using extended fields.
New API endpoints can be added to enable software to efficiently make use of the new features, and that can be done at any time, before or after the upgrade.
Exception is the "raw" transaction API discussed above, where even unupgraded software would get to see the extended fields.

### Network Rules

This proposal changes nothing in the message format and only requires update of the network "standardness" rule to allow the grouped output templates.
This is a cost contained to node software, whether they validate transactions or not.

### Group Consensus Logic

The proposal requires validating nodes to activate additional code paths in order to verify outputs whose `unlockingScript` or lockingScript` starts with the `PFX_GROUP_GENESIS` or `PFX_GROUP` "magic" byte `0xEE`.
Implementing the rules is a cost contained to validating node software.
Transaction validation costs are part of the ex-post costs, discussed in the next section.

## Ongoing Costs

[[Back to Contents](#contents)]

We define ongoing cost as any cost that would arise post upgrade.
They could be simply an addition to node operating costs, or development costs to support new features that would become viable post activation.
Here we have to make an important distinction, and we will define two broad categories:

1. Unavoidable costs, such as node operating costs required to validate new rules.
2. Opt-in costs, such as block explorer upgrade and operating costs to support native tokens ecosystem.

We don't see a significant increase in unavoidable costs and on that we will elaborate below.
With opt-in costs, it is reasonable to expect that those will be part of some economic calculation which would result in a net benefit.
We will discuss both nevertheless.

### Unavoidable Ongoing Costs

These will be contained to node software, and consist of:

- Node software maintenance costs.
- Node operating costs.

#### Node software maintenance costs

The proposal has a clear logical structure and can be entirely implemented in procedural programming paradigm.
Therefore, we posit that it is easy to reason about and we do not see this proposal as introducing potential surprises to those who would later maintain the code.

Still, some consideration will have to be given to existence of group code.
For information, listed below are some changes that would NOT be significantly affected by this proposal's implementation:

- Network rules, unless we choose to give special treatment to grouped UTXOs;
- Block and transaction size limits, unless we choose to give special treatment to transactions containing grouped outputs;
- Pruning;
- New opcodes;
- New signature schemes;
- UTXO commitments.

Some future upgrades might be affected:

- Changing numerical precision for BCH & tokens, and it would be possible to change BCH precision independently of tokens;
- Totally new state scheme e.g. [utreexo](https://dci.mit.edu/utreexo).

#### Node operating costs

Impact on node operating costs can be analyzed through:

1. Network bandwidth requirements.
2. Node local storage requirements.
3. Node CPU time required for transaction validation.

##### Additional network bandwidth requirements

Group annotation adds 41 bytes to each grouped BCH output.
Size of inputs spending grouped outputs is not increased.
Pure BCH outputs can be mixed with grouped outputs in the same transaction.
Only outputs which are grouped will add some extra data to transactions.

In general, transactions will be `nGOut * 42` bigger than comparable non-grouped pure BCH transactions, where `nGOut` is the number of *grouped* outputs in a transaction.

When discussing transaction sizes, this is no different than some custom input script, increasing the number of multisig keys, or OP_RETURN protocols.
The user who has a need for the extra feature has to add some more data to the transaction which encodes his use of the feature, and for that he pays the miner's fee.
Difference is in that grouped outputs require storing their state in the "premium" part of the blockchain, the UTXO set, which we will discuss below.

##### Node local storage requirements

When a node performs transaction validation then, in addition to the data provided by the transaction itself, it must also fetch each referenced UTXO from the database so that it can access previous output's satoshi amount and locking script.
This holds the same for both pure BCH outputs and grouped outputs.
No new data structures are required, and no additional database access operations are required.

It is only the size of the `lockingScript` field which is increased, and that field is part of the UTXO set, for which nodes already build and maintain an index.
Consensus rules already support `lockingScript` sizes of variable length, and even network rules allow a variable length with the P2MS output template.

##### Node CPU time required for transaction validation

Group FORMAT rule is trivial to verify, as it is simply verifying the structure of the `lockingScript` field.

Group genesis and the native token super-contract requires more complex operations, and we will discuss those below.

###### Group Genesis Transactions

As discussed above, transaction verification requires hashing (SHA-256) additional `146*n` bytes, where `n` is the number of genesis inputs appearing in the transaction.

This is insignificant when compared to CPU load required to verify input signatures or redeem scripts that can execute hash operations on orders of magnitude more bytes.

###### Group super-contracts

Validation of group super-contracts requires no cryptographic operations.
It requires only simple operations such as comparison, arithmetic, and bitwise operations.
It also requires some transaction-local temporary indexes to be built in memory that would store distinct `groupID`s seen in the transaction being validated.
Required operations can be observed in [Appendix - Group Logic Pseudo Code](#appendix-group-logic-pseudo-code).

Upper bound of the temporary index can be calculated from current consensus limits on transaction size.
Outputs have less overhead and are therefore a cheaper way to inflate the number of possible distinct `groupID`s.
Consensus limits transaction size to 1MB, and the smallest input is an anyone-can-spend with `OP_1` as the signature, which would be 42 bytes.
Smallest output would be an amountless group type with some P2SH output script, which would be 66 bytes.
From that we can calculate the maximum number of distinct `groupID`s in a transaction:
`(1000000-42)/66 == 15150`.
Index building operations are very cheap when compared to hashing and of `O(n*log(n))` time complexity.
With the above transaction size limit the `log(n)` can't exceed a factor of 4.

We could argue that presence of group annotation in fact reduces CPU load per byte, as the annotation is non-executable "dumb" data.

### Opt-in Ongoing Costs

These can be thought of as cost of accessing the new features.
For example, a wallet doesn't need to support new features and cost to that wallet's developers and users will be 0 unless they want to access the new features.

#### P2PKH and P2MS Native Token Users

Wallets will need to add the group P2PKH and P2MS templates and then they will be able to see grouped outputs, this is a trivial parsing task.
To be able to actually spend them, the wallets will need to implement and maintain native token group logic, so that they can construct transactions that would satisfy the super-contract rules.

#### P2SH Users

Updates will be required to some libraries, transaction compilers, and other software which implements the BCH VM (e.g. Bitauth IDE).

#### SPV Servers

Without a change to SPV servers, upgraded wallets could still use the new feature by subscribing to the `lockingScript` as it would include the group annotation.
This would come with a caveat that wallets would need to decide what token IDs they'll subscribe for.

Explicitly supporting new features would require an upgrade to SPV server software, so that the `lockingScript` would not include the group annotation.
That way, it would notify wallets of any amount received to a particular address, BCH or token.

#### Other Software

Block explorers, exchanges, indexers, etc. may be running some custom node-dependent software.
While this upgrade wouldn't break their existing functionality, accessing the new features would require an upgrade of their software stack.

#### Ecosystem Building

We hope to see standards emerge around native token use, such as NFT standards, metadata extensions, and standardization of baton contracts placed on token genesis prevouts.
Developing such standards is also a cost to be recognized, albeit one that is expected to be offset by ecosystem-wide benefits.

## Costs and Benefits Summary

[[Back to Contents](#contents)]

General costs and benefits:

- (+) Efficiency and scalability advantage of UTXO native tokens in comparison to account-based blockchains.
- (+) Enabling efficient UTXO powered DeFi.
- (+) Attracting new users, talent, and capital thus helping grow the entire ecosystem centered around Bitcoin Cash.
- (+) Future proof.
Since every group token is also a BCH output, it should seamlessly work with everything that can work with BCH, therefore native tokens can reap benefits from future upgrades.
- (+) Every grouped output created also creates marginal demand for BCH.

### 1 Node Stakeholders

- (-) One-off costs of upgrading node software (coding, reviewing, testing, deployment).
- (0) No impact on operating costs, scaling "big O" unchanged.

#### 1.1 Miners

- (+) More fee revenue through increased demand for transactions.

#### 1.2 Big nodes

Exchanges, block explorers, backbone nodes, etc.

- (+) Easy to implement other currencies through same familiar design patterns that are now used for Bitcoin Cash alone.

#### 1.3 User nodes

- (0) No impact.

#### 1.4 Node developers

- (-) One-off workload increase.
- (-) Future maintenance cost.
- (+) Later it just works, downstream developers create utility.

### 2 Userspace stakeholders

- (+) Barrier to entry to token usage lowered.

#### 2.1 Business users

- (+) Access to financial instruments previously not available, or access to them at a better price and quality than competition can provide.
- (+) Enables potential new business models.

#### 2.2 Userspace developers

- (+) Easy to implement other currencies through the same familiar design patterns that are now used for Bitcoin Cash alone.
- (+) With more growth there will be more opportunity for professional advancement within the ecosystem.

#### 2.3 Individual users

- (+) Get to enjoy the best peer-to-peer electronic cash system!

### 3 Holders and speculators

#### 3.1 Long

- (+) Adoption correlates with increase in price.
- (+) Even other tokens marginally increase demand for BCH, so the more
transactions of any kind, the better.
- (+) More liquidity in the market.

#### 3.2 Short

- (-) Adoption correlates with increase in price.
- (+) More liquidity in the market.

### 4 Opinion holders who are not stakeholders

- (0) We are hoping developments like this will move them towards becoming stakeholders!

## Risk Assessment

[[Back to Contents](#contents)]

If we define risk as [probability times severity](https://en.wikipedia.org/wiki/Risk#Expected_values) then we could make an estimate using the [risk matrix](https://en.wikipedia.org/wiki/Risk_matrix).

Given the layer on which this is to be implemented, the probability of bugs and network splits is entirely controlled by node developers and quality of their work.
After the node software QC process is completed, the probability of a bug should be very low.

Severity would depend on nature of the bug.
Because this proposal is well contained and introduces only additional constraints on the system, severity would be limited to grouped outputs users.

## Evaluation of Alternatives

[[Back to Contents](#contents)]

### Script Tokens

Script token contracts that want to do conceptually simple things would have to be complicated and limited in number of inputs/outputs that they could support.
For example, an NFT contract would require over 300 bytes of unlocking script that would have to be repeated with every transfer, just to prove that an NFT is an NFT.

With this proposal, it is possible to remove the burden on Script and users by providing consensus-enforced super-contracts which can interface with the Script layer, retaining flexibility of Script while allowing easy and efficient P2PKH tokens.

### Other Blockchains

The below table compares the proposed token solution with other popular blockchains.

||ERC-20|Native tokens|Native tokens|
|---|---|---|---|
|Backing blockchain|Ethereum|Cardano|Bitcoin Cash|
|Relationship to the blockchain|A contract standard, users copy-paste the standard code and modify it.|Not a standard. Most functionality is built into the ledger itself.|Not a standard. Most functionality is built into the ledger itself.|
|Controlled by|A Solidity smart contract|A minting policy script in any scripting language supported by Cardano|A minting policy Bitcoin Cash Script|
|Requires a smart contract to mint/burn?|Y|Y|Optional, by placing a P2SH Script contract on the genesis output|
|Minting logic can be customized?|Y|Y|Y|
|Requires a smart contract to transfer?|Y|N|N|
|Can be used by other smart contracts without special support?|N|Y|Y|
|Can be transferred alongside other tokens?|N|Y|Y|
|Transfer logic provided by?|Copy-pasting from the ERC-20 template|The Cardano ledger itself|The Bitcoin Cash ledger itself|
|Transfer logic can be customized?|Y|N|Y, by having the minting contract require that minted amounts inherit a P2SH contract|
|Requires special fees to transfer?|Y|N|N|
|Requires additional event-handling logic to track transfers?|Y|N|N|
|Supports non-fungible tokens?|N|Y|Y|
|Human readable metadata|Provided by the operating smart contract|Provided by the off-chain metadata server|Provided by the off-chain metadata server; Metadata updating/locking contracts possible|

Note: table adapted from [here](https://cardano-ledger.readthedocs.io/en/latest/explanations/features.html#how-do-native-tokens-compare-to-erc-20-tokens).

### Industry Acceptance

#### Cardano

Cardano blockchain provides native tokens which can be used in contracts.

>Native tokens behave the same as ada in most ways. However, Ada is the “principal” currency of Cardano, and is the only one which can be used for some special purposes, such as paying fees.

|                                               |Ada |Native tokens | Comment                                                                           |
| --------------------------------------------- | -- | ------------ | --------------------------------------------------------------------------------- |
|Can be sent in transactions                    | Y  | Y            |                                                                                   |
|Can be kept in UTXO outputs                    | Y  | Y            |                                                                                   |
|Can be locked with script outputs              | Y  | Y            |                                                                                   |
|Can be sent to an exchange address             | Y  | Y            |                                                                                   |
|Can be minted/burned                           | N  | Y            | Ada cannot be created or destroyed, its policy ID does not correspond to a script |
|Can be used to pay fees, receive rewards, etc. | Y  | N            | Ada is the only currency which can be used for fees and rewards.                  |
|Can be used to cover the minimum UTXO value    | Y  | N            | Ada is the only currency which can be used for deposits.                          |

[Link to source.](https://cardano-ledger.readthedocs.io/en/latest/explanations/features.html#how-do-native-tokens-compare-to-ada)

The same table could be used to summarize proposed Bitcoin Cash native tokens, simply by swapping ADA with BCH.

#### IOTA

The proposed [IOTA tokenization](https://blog.iota.org/tokenization-on-the-tangle-iota-digital-assets-framework/) is a functional subset of Group Tokenization, but notably uses the exact same technique of annotating UTXOs with token identifiers.

#### ION

[ION tokens](https://ionomy.com/) are ported directly from Andrew Stone's work.

#### Blockstream Liquid

["Confidental Assets"](https://blockstream.com/bitcoin17-final41.pdf) uses the same general approach of "...attaching to each output an asset tag identifying the type of that asset, and having verifiers check that the verification equation holds for subsets of the transaction which have only a single asset type."

## Discussions

[[Back to Contents](#contents)]

- [Group CHIP Discussion](https://bitcoincashresearch.org/t/chip-2021-02-group-tokenization-for-bitcoin-cash/311)
- [Old Group Proposal Discussion](https://bitcoincashresearch.org/t/native-group-tokenization/278)
- [Discussion on Interaction With Other CHIPs](https://bitcoincashresearch.org/t/brainstorming-interaction-between-group-pmv3-and-introspection/430)

## Statements

[[Back to Contents](#contents)]

>A lot of work and research has gone into the technical specification of this CHIP. However, in its current form, GP does not think it is simple / minimal enough to be considered for acceptance into the network. Furthermore, important sections about costs, risks and alternatives are incomplete or have wrongfully been asserted as non-existent. GP would like to see a minimum viable change isolated from the current large scope, and then a deeper evaluation of costs, risks and alternatives.

<a href="https://read.cash/@GeneralProtocols/general-protocols-statement-on-chip-2021-02-group-tokenization-for-bitcoin-cash-e7df47a0"><p align="right">2021-04-02, General Protocols, General Protocols Statement on "CHIP-2021-02 Group Tokenization for Bitcoin Cash"</p></a>

>GROUP tokens are by far the most streamlined proposal to achieving token-supported covenants which could definitely help Bitcoin Cash unlocking its potential to mass adoption. Group tokens are not only needed for gaining user adoption, but also developer adoption. Builders need more scripting capabilities to building meaningful dapps of which Group proposal could definitely contribute to.

<a href="https://youtu.be/f9wTZzNk1ro?t=4319"><p align="right">2021-04-05, Burak, SwapCash</p></a>

>As a developer and co-founder of Mint SLP https://mintslp.com/
>
>I believe miner validated tokens is a smart step in the right direction! The Group Token proposal seems like a big improvement over SLP.
>
>Thank you @andrewstone and others involved for working on and pushing this feature into BCH.
>
>I hope the community can come together to support the Group Token proposal or something similar! You have my support for what it is worth!

<a href="https://bitcoincashresearch.org/t/chip-2021-02-group-tokenization-for-bitcoin-cash/311/29?u=bitcoincashautist" align=right><p align="right">2021-04-06, MaxHastings, Mint Slp</p></a>

## Changelog

[[Back to Contents](#contents)]

This section summarizes the evolution of this document.

Note that, through interaction with various stakeholders of the Bitcoin Cash ecosystem, this proposal has [significantly deviated](#changelog) from past versions which were made to match Andrew Stone's [original proposal](https://docs.google.com/document/d/1X-yrqBJNj6oGPku49krZqTMGNNEWnUJBRFjX7fJXvTs).

Shortly after v6.0 release of this proposal, another proposal, [CHIP-2022-02-CashTokens: Token Primitives for Bitcoin Cash](https://github.com/bitjson/cashtokens/blob/master/readme.md#chip-2022-02-cashtokens-token-primitives-for-bitcoin-cash), was released by Jason Dreyzehner.

From that point, this proposal started moving in direction of converging on a solution to be finally activated on the Bitcoin Cash network, by implementing revelations found in CashTokens and with the ultimate goal of merging the two proposals to enable token primitives for Bitcoin Cash.

The key revelation of CashTokens was that non-fungible tokens (NFTs) and fungible tokens (FTs) must be allowed to [independently exist](https://github.com/bitjson/cashtokens/blob/master/readme.md#incompatibility-of-token-fungibility-and-token-commitments) within the same group because NFTs are more than FTs with amount=1 and fundamental use of NFTs is as messengers between smart contracts.

**2022-06-05** 7.0 Native Token Primitives

- Converge towards "CHIP-2022-02-CashTokens: Token Primitives for Bitcoin Cash" by implementing its NFT approach
- Repurpose `groupType` to `groupOutputType`
- Allow groups to have both NFTs and FTs under the same `groupID`
- Allow grouped outputs to encode FTs, an NFT, or both
- Implement NFT sub-types
- Allow NFTs to carry a 0-40 byte message
- Functionally matches CashTokens 2.0, main difference is the genesis setup, and a minor difference is output encoding

**2022-02-28** [e148a4dc](https://gitlab.com/0353F40E/group-tokenization/-/commit/e148a4dce1ea3363f7a36e9fdf13ef8f5cb0cfd5) 6.1 Fungible Tokens

- Acknowledge existence of "CHIP-2022-02-CashTokens: Token Primitives for Bitcoin Cash" and start working towards merging the two proposals
- Extract `groupType` to its own field to enable signing group genesis from the redeem Script without the problem of malleating the 
nonce
- Align `groupType` with [CT2.0](https://github.com/bitjson/cashtokens/tree/fcb110c3309901886b2c7d3417568d8b13fb01b5#prefix_token)
- Remove singularity amount overload, entire fungible token supply of the group must be minted at genesis
- Modify genesis - prevout hash now optional

**2022-02-21** [f11857a5](https://gitlab.com/0353F40E/group-tokenization/-/commit/f11857a54fde8a81d0df5f026a26fa650f5aeab1) 6.0 Explicit Genesis

- Whole new approach to group genesis: instead of inferring it from the TX, it will be explicitly declared as an input prefix that can use the same byte because it will be exclusive to input context.
- One more introspection opcode to access the genesis input's generated `groupID` preimage.
- Change group amount format from VarInt to fixed width uint.

**2022-02-12** [7fd63e09](https://gitlab.com/0353F40E/group-tokenization/-/commit/7fd63e0992a20d1ab271956ae7a105e58c9fa66c) 5.0 Simplify the spec, CHIP sections

- NFT and satoshi tokens group types dropped.
- Only native tokens group type is allowed.
- Genesis simplified.
- Removed the examples appendix.

**2022-01-25** [09e93d79](https://gitlab.com/0353F40E/group-tokenization/-/commit/905cac9e4afe9393f1e0339eafc65386dfb12090) 4.3 Spec tweaks, CHIP sections

- Remove the `groupAmount == 1` requirement for satoshi tokens so contracts can make use of the free `groupAmount` field.
- Tweak group introspection opcodes so we can use them to check for `groupAmount` existence.
- Tweak "singularity" amount encoding and with that limit the `groupAmount` range to match script numbers range.
- Remove option to change TX format.
- Remove `NFT | HAS_AMOUNT` group flag combination, keep it reserved for a more versatile NFT upgrade.
- Change `groupType` to 1 byte width, define a flag to extend it to 2 bytes if needed.
- Sighash and genesis preimage tweaks: mask genesis generated bits instead of omitting them.
- Genesis now increments SigChecks counter.
- Big update to CHIP outline and sections, mainly costs and evaluation of alternatives sections.

**2021-12-21** [5b10c05b](https://gitlab.com/0353F40E/group-tokenization/-/commit/5b10c05b8adad7b1a5b2c51d3950a2e91378a57e) 4.2 Tweak GENESIS preimage

**2021-12-08** [3090f8a2](https://gitlab.com/0353F40E/group-tokenization/-/commit/3090f8a27d6c9a588e42113c638cb906a0d58b86) 4.1 Tweak GENESIS and add NFT group flag

- Add a NFT group flag so we can have NFTs that carry some state
- Tweak GENESIS to compress each input in the preimage
- Add PMv3 GENESIS and SIGHASH sections
- Wording

**2021-11-29** [b96c2f35](https://gitlab.com/0353F40E/group-tokenization/-/commit/b96c2f35dd551e7fa9e8420d19d72e45bfeac50c) 4.0 Unforgeable Groups

- Simplified the output format and consensus layer
- Generalized output groups as "carried digest", having only the GENESIS rule enforced
- Group amount field is now optional and indicated using the LSB of `groupID` so the whole group must have or not have it
- Native token groups and Satoshi token groups (aka "holds BCH") are then a more restricted variant of a generalized group, having token logic enforced over respective amount fields, and NFT logic enforced over amountless token groups.
- 0-amount indicates "singularity", i.e. an infinite source or an infinite sink which allows token amount creation and destruction
- Any advanced features, such as metadata updating, are to be implemented using unforgeable Script covenants, as demonstrated in the examples section
- Reworked CHIP sections

**2021-11-11** [2904667d](https://gitlab.com/0353F40E/group-tokenization/-/commit/2904667daa32f0cf9dce862c916ea6305f2fdf88) 3.1 Sections Overhaul

- Added back MELT and infinite MINT
- Tweaked authority-type NFTs
- Tweaked the tokenID preimage to enable Script covenants
- Renamed "baton" back to "authority", renamed metadata LOCK_X to EDIT_X
- Reworked CHIP sections: made motivation more concise, added intro section intended for wider audience, added contents, changelog, reworked technical description, updated examples, added example Script covenant contract...

**2021-05-09** [8c9cb4a2](https://gitlab.com/0353F40E/group-tokenization/-/blob/8c9cb4a23364e3bba3a87319f1ad068065735530/CHIP-2021-02_Group_Tokenization_for_Bitcoin_Cash.md) 3.0 One Token Standard

- Result of brainstorming with Emil Oldenburg
- Authority system restricted to exactly one UTXO per token
- Introduced metadata locks

**2021-04-17** [4be89a3b](https://gitlab.com/0353F40E/group-tokenization/-/blob/4be89a3b7b10329536bbd3367b87e2405df39e81/CHIP-2021-02_Group_Tokenization_for_Bitcoin_Cash.md) 2.0 Full Overhaul

- Original scope sliced down in response to feedback received
- All advanced features removed
- Reworked technical description to flow better
- Included group consensus rules flowchart

**2021-03-14** [fa4964d2](https://gitlab.com/0353F40E/group-tokenization/-/blob/fa4964d2099c0e62423b8f392eabeb25df8518bc/CHIP-2021-02_Group_Tokenization_for_Bitcoin_Cash.md) 1.1 Completed the First Draft

- Copied Andrew Stone's [specification](https://github.com/bitcoin-unlimited/BUwiki/commit/880d69330623e4eb4e0b068c7361c6c60494e9e7) into the CHIP body.
- Reworked technical description to include advanced Group features: subgroups, hold BCH, and covenant.

**2021-02-23** [646232cc](https://gitlab.com/0353F40E/group-tokenization/-/blob/646232cc628e65bc624ca8a455d17e3db16bfbd8/CHIP-2021-02_Group_Tokenization_for_Bitcoin_Cash.md) 1.0 First Draft, CHIP [published](https://bitcoincashresearch.org/t/chip-2021-02-group-tokenization-for-bitcoin-cash/311/1) on Bitcoin Cash Research forum

- CHIP created to describe Andrew Stone's [original specification](https://github.com/bitcoin-unlimited/BUwiki/blob/e9f1956657c375b6eb2c5b85b5b1c868e5db839c/grouptokenization/groupbchspec.md).
- First draft technical description incomplete, introduced only the token transfer, authority, and genesis features.

## Copyright

[[Back to Contents](#contents)]

To the extent possible under law, this work has waived all copyright and related or neighboring rights to this work under [CC0](https://creativecommons.org/publicdomain/zero/1.0/).

## Credits

[[Back to Contents](#contents)]

Satoshi Nakamoto, invention of [Bitcoin: A Peer-to-Peer Electronic Cash System](https://www.bitcoin.com/bitcoin.pdf).  
Andrew Stone, authoring the [original Group Tokenization proposal](https://www.nextchain.cash/grouptokens) and full showcase implementation.

The CHIP however, is really authored by everyone who's interacted with it.
I'd like to personally thank:

- Andrew Stone, for inspiration, being supportive of the initial CHIP versions, and private consultations,
- Thomas Zander, for inspiration, being the "first responder" to the proposal, and helping me realize the importance of attention to details and their impact on the full product,
- matricz, for the [inspiring post](https://read.cash/@mtrycz/how-i-envision-decentralized-cooperation-on-bitcoin-cash-9876b9e9) on how decentralized cooperation should work,
- imaginary_username, for looking at it with a critical mind and helping me find a better way forward,
- BigBlockIfTrue, for early interaction with the proposal and explaining to me the nuances of locking and unlocking script,
- emergent_reasons, for setting the bar high for CHIPs and asking for steel-man arguments,
- Calin Culianu, for kicking the CHIP into higher gear with the PFX_GROUP approach,
- Emil Oldenburg, for having the patience to help me see the advantage of having a single authority and enforcing metadata locks.
- Jason Dreyzehner, for breakthrough revelation with CashTokens2.0, that of fundamental differences between fungible and non-fungible tokens

and everyone else who interacted with this proposal and by doing so helped bring it to the level where it currently is:

Sorted alphabetically: AsashoryuMaryBerry, Burak, Estebansaa, FerriestaPatronum, Fixthetracking, Freedom-phoenix, Freetrader, George Donelly, Jonathan Silverblood, Licho, Marc De Mesel, MobTwo, Mr. Korrupto, Saddit42, Shadow Of Harbringer, Supremelummox, Tl121, Tula_s, Twoethy.

If I forgot someone, please let me know!

## Appendix - Group Logic Pseudo Code

[[Back to Contents](#contents)]

```
for (each transaction) {
    Verify Group FORMAT and TYPE rules during deserialization
    Verify existing Bitcoin Cash consensus rules

    for (each input in tx.inputs) {
        Verify existing Bitcoin Cash consensus rules
        // Additionally...

        // Tally input balances for each encountered group
        // Genesis inputs
        with (input) {
            if (isGroupGenesis()) {
                tx.groups.add(genGroupID)
                if (genGroupType == 0x00)
                    if (tx.groups(genGroupID).inputGroupSum.safeAdd(genGroupAmount) == false)
                        fail()
            }
        }
        // Grouped prevouts
        with (input.vout) {
            if (isGrouped()) {
                tx.groups.add(groupID)
                if (groupType == 0x00)
                    if (tx.groups(groupID).inputGroupSum.safeAdd(groupAmount) == false)
                        fail()
                }
            }
        }

        Verify the rest of existing Bitcoin Cash consensus rules
    }

    for (each output in tx.outputs) {
        Verify existing Bitcoin Cash consensus rules
        // Additionally...
        with (output) {
            if (isGrouped()) {
                if (tx.groups.exists(groupID) == 0)
                    fail()
                tx.groups(groupID).outputCount++
                if (groupType == 0x00) {
                    if (tx.groups(groupID).outputGroupSum.safeAdd(groupAmount) == false)
                        fail()
                }
            }
        }
        Verify the rest of existing Bitcoin Cash consensus rules
    }

    // Group consensus logic
    for (each group in tx.groups) {
        with (group) {
            if(outputCount < 1)
                fail()
            if (groupType == 0x00)
                if(inputGroupSum != outputGroupSum)
                    fail()
            }
        }
    }

    Verify the rest of existing Bitcoin Cash consensus rules
}
```
